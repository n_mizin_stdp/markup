$(document).ready(function(){
	var version = navigator.userAgent;  // ----- Переменная с данными о браузере
	// -------------------------------------------------------------------------------
	var chrome = version.search(/Chrome/i),   //
		operaAdd = version.search(/OPR/i),    //
		mozilla = version.search(/Firefox/i), // --------------- Переменные возвращающие порядковый номер символа на котором заканчиваются ключивые слова
		ie = version.search(/MSIE/i),         //
		opera = version.search(/OPR/i),       //
		safari = version.search(/Version/i);  //

	// ------------------------------------------------------------ Определяем версию браузера опера
		var operaIdentification = function(){
			if(opera == -1){
				return;
			}else{
				var operaIndex = opera + ('OPR').length + 1;
				var operaVersion = version.substr(operaIndex, 2);

				if(operaVersion < 20){
					alert('Старый браузер');
				}else{
					alert('У вас установлена ' + operaVersion + ' версия браузера, обновление не требуется');
				};
			};
		};
	// ------------------------------------------------------------- Определяем версию браузера сафари

		var safariIdentification = function(){
			if(safari == -1){
				return;
			}else{
				var safariIndex = safari + ('Version').length + 1;
				var safariVersion = version.substr(safariIndex, 2);

				if(safariVersion < 8){
					alert('У вас старый браузер, обновите его');
				}else{
					alert('У вас установлена ' + safariVersion + ' версия браузера, обновление не требуется');
				};
			};
		};
	// ------------------------------------------------------------ Определяем версию браузера chrome
		var chromeIdentification = function(){
			if(chrome == -1 || operaAdd !== -1){
				return;
			}else{
				var chromeIndex = (chrome + ('Chrome').length + 1);
				var chromeVersion = version.substr(chromeIndex, 2);
				
				if(chromeVersion < 35){
					alert('Старый браузер');
				}else{
					alert('У вас установлена ' + chromeVersion + ' версия браузера, обновление не требуется');
				};
			};
		};
	// -------------------------------------------------------------- Определяем версию браузера мозиллы
		var mozillaIdentification = function(){
			if(mozilla == -1){
				return;
			}else{
				var mozillaIndex = (mozilla + ('Firefox').length + 1);
				var mozillaVersion = version.substr(mozillaIndex, 2);
				if(mozillaVersion < 30){
					alert('Старый браузер');
				}else{
					alert('У вас установлена ' + mozillaVersion + ' версия браузера, обновление не требуется');
				};
			};
		};
	// ----------------------------------------------------------------- Определяем версию браузера IE
		var explorerIdentification = function(){
			if(ie == -1){
				return;
			}else{
				var ieIndex = (ie + ('MSIE').length + 1);
				var ieVersion = version.substr(ieIndex, 2);
				
				if(ieVersion < 10){
					alert('Старый браузер');
				}else{
					alert('У вас установлена ' + ieVersion + ' версия браузера, обновление не требуется');
				};
			};
		};

//---------------------------------------------------------------- Init functions

		operaIdentification();
		safariIdentification();
		chromeIdentification();
		explorerIdentification();
		mozillaIdentification();



});