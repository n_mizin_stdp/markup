var gulp = require('gulp');
var concatCss = require('gulp-concat-css')
var minifyCss = require('gulp-minify-css');
var jade = require('gulp-jade');
var compass = require('gulp-compass');
var rename = require("gulp-rename");
var htmlmin = require('gulp-htmlmin');


var jadePath = 'app/**/*.jade';
var scssPath = 'sass/*.scss';
var htmlPath = 'app/*.html';




 
gulp.task('templates', function() {
  var YOUR_LOCALS = {};
 
  gulp.src('app/jade/*.jade')
    .pipe(jade({pretty: '\t'}))
    .pipe(gulp.dest('app'))
});

gulp.task('minify', function() {
  return gulp.src('app/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/workHtml'))
});





gulp.task('compass', function() {
  gulp.src(scssPath)
    .pipe(compass({
      config_file: 'config.rb',
      css: 'app/css',
      sass: 'sass',
      // image: 'app/images'
    }))     
    .pipe(gulp.dest('app/css'));
});




//css
gulp.task('default', function () {
  return gulp.src('app/css/*.css')
    .pipe(concatCss("workCss/bundle.css"))
    .pipe(minifyCss("workCSS/bundle.css"))
    .pipe(rename('workCSS/bundle.min.css'))
    .pipe(gulp.dest('app/'))

});




gulp.task('html', function (){
	gulp.src('app/index.html')
});





gulp.task('watch', function(){
	gulp.watch('app/css/*css',['default'])
  gulp.watch(jadePath, ['templates'])
  gulp.watch(scssPath, ['compass'])
  gulp.watch(htmlPath, ['minify'])

});