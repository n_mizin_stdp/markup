$(document).ready(function(){

// ------------- Маска для поля телефона

	$('.complaint-phone').mask("+7 (999) 999-99-99");

// ------------- Антибот

	$('.complaint-antibot-box').on('click', function(){
		var $this = $(this);
			if (!$this.hasClass('antibot-box-white')) {
				$(this).addClass('antibot-box-white');
			}
	});

    var complaint_form = $('#complaint-form');


    complaint_form.on('submit', function(e){
		e.preventDefault();
			var $this = $(this),
				$bot  = $('.complaint-antibot-box'),
				$required = $('.required-input');

				if ($bot.hasClass('antibot-box-white')) {
					console.log('send form');
				}

		return false;
	});


// -------------- Обрабатываем всплывашки для input
    complaint_form.on('submit', function(){
		$('.complaint__inner-box').each(function(i, item){
				var current = $(this),
					input = current.find('.required-input');

					if (input.val() == '') {
						current.append('<div class="input-error"> - Обязательное поле</div>');
						resetError(current);
					}  

		});

// ----------------- Обрабатываем всплывашки для textarea

		$('.complaint__inner-box-textarea').each(function(i, item){
				var current = $(this),
					input = current.find('.complaint-textarea');

					if (input.val() == '') {
						current.append('<div class="input-error"> - Обязательное поле</div>');
						resetError(current);
					}


		});  

	});

// ------------ Удаляем всплывающие подсказки
	function resetError(object) {
		if (object !== undefined) {
			object.on('keypress', function() {
				var _this = $(this);
					_this.find('.input-error').fadeOut(function() {
						$(this).detach();
					});
			});	
		}		
	}

});
